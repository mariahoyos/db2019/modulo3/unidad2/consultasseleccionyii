<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ciclistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ciclista-index">
    <div class="jumbotron">
        <h2>- Listar las edades de los ciclistas (sin repetidos) -</h2>
        <div>SELECT DISTINCT edad FROM ciclista</div>
    </div>
    <?= GridView::widget([
        'dataProvider' => $datos,
        'columns'=>[
            'edad'
        ],
    ]); ?>


</div>
