<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ciclistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ciclista-index">
    <div class="jumbotron">
        <h2>- Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30 -</h2>
        <div>SELECT dorsal FROM ciclista WHERE edad<24 OR edad>30</div>
    </div>
    <?= GridView::widget([
        'dataProvider' => $datos,
        'columns'=>[
            'edad'
        ],
    ]); ?>


</div>
