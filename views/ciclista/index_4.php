<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ciclistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ciclista-index">
    <div class="jumbotron">
        <h2>- listar las edades de los ciclistas de Artiach o de Amore Vita -</h2>
        <div>SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'</div>
    </div>
    <?= GridView::widget([
        'dataProvider' => $datos,
        'columns'=>[
            'edad'
        ],
    ]); ?>


</div>
