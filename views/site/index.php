<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Consultas de Selección 1';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de Selección 1</h1>
        <p class="lead">Hoja de ejercicios de MySql</p>
    </div>

    <div class="body-content">

        <div class="row">
            <!-- En bootstrap se organizan por filas y columnas
            en total hay 12 columas
            col es para que empiece la columna
            lg es para pantallas grandes
            4 es para que ocupa 4 de las 12 columnas totales
            -->
            <div class="col-lg-4">
                <h2>Consulta 0</h2>
                <?= Html::a('Ejecutar Consulta', ['ciclista/consulta'], ['class' => 'btn btn-info btn-large']) ?>

            </div>
            <div class="col-lg-4">
                <h2>Consulta 1</h2>
                <?= Html::a('Ejecutar Consulta', ['ciclista/consulta1'], ['class' => 'btn btn-info btn-large']) ?>

            </div>
            <div class="col-lg-4">
                <h2>Consulta 2</h2>
                <?= Html::a('Ejecutar Consulta', ['ciclista/consulta2'], ['class' => 'btn btn-info btn-large']) ?>

            </div>
        </div>
        <div class="row">
              <div class="col-lg-4">
                <h2>Consulta 3</h2>
                <?= Html::a('Ejecutar Consulta', ['ciclista/consulta3'], ['class' => 'btn btn-info btn-large']) ?>

            </div>
            
            
            
              <div class="col-lg-4">
                <h2>Consulta 4</h2>
                <?= Html::a('Ejecutar Consulta', ['ciclista/consulta4'], ['class' => 'btn btn-info btn-large']) ?>

            </div>

        </div>
    </div>

</div>
</div>
