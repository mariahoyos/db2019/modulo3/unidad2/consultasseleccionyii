<?php

namespace app\controllers;

use Yii;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CiclistaController implements the CRUD actions for Ciclista model.
 */
class CiclistaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    
    /**
     * Lista todos los ciclistas
     * utilizando ActiveRecord
     * @return type view
     */
    public function actionConsulta(){
        //select * from ciclista
        $consulta=Ciclista::find();  
        
        $dataProvider = new ActiveDataProvider([
            'query' =>$consulta,
        ]);

        return $this->render('index_1', [
            'datos' => $dataProvider,
        ]);
    }
    
    /**
     * Consulta1 - Consultas de selección 1
     * Lista las edades de los ciclistas (sin repetidos)
     * @return type view
     */
    
    /*
     * Método Consulta1: SELECT EDAD FROM CICLISTA
     */
    public function actionConsulta1(){
        $consulta=Ciclista::find()->select("edad")->distinct(); 
        
        /*

         * activeRecord - Ciclista
         * ActiveQueryInterface - Ciclista::find() que hereda de ActiveQuery
         */
        
        $dataProvider = new ActiveDataProvider([
            'query' =>$consulta,
        ]);

        return $this->render('index_2', [
            'datos' => $dataProvider,
        ]);
    }
    
    public function actionConsulta2(){
        $consulta=Ciclista::find()
                ->select("edad")
                ->distinct()
                ->where(["nomequipo"=>'Artiach']); 
             
        $dataProvider = new ActiveDataProvider([
            'query' =>$consulta,
        ]);

        return $this->render('index_3', [
            'datos' => $dataProvider,
        ]);
    }
    
    public function actionConsulta3(){
        $consulta=Ciclista::find()->
                select("edad")->
                distinct()->
                where("nomequipo='Artiach' or nomequipo='Amore Vita'"); 
             
        $dataProvider = new ActiveDataProvider([
            'query' =>$consulta,
        ]);

        return $this->render('index_4', [
            'datos' => $dataProvider,
        ]);
    }
    
    public function actionConsulta4(){
        $consulta=Ciclista::find()->
                select("edad")->
                distinct()->
                where("edad<24 or edad>30"); 
      
             
        $dataProvider = new ActiveDataProvider([
            'query' =>$consulta,
        ]);

        return $this->render('index_5', [
            'datos' => $dataProvider,
        ]);
    }
    
    
    
    
    /**
     * Lists all Ciclista models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ciclista model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ciclista model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ciclista();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->dorsal]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ciclista model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->dorsal]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ciclista model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ciclista model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ciclista the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ciclista::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
